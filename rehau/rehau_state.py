
import asyncio
#import airsensor

import logging
import usb.core
import usb.util

logger = logging.getLogger('iAQ_Stick')

class AirSensor():

    initialized = False

    ppm = 0

    def xfer_type2(self, msg):
        out_data = bytes('@', 'utf-8') + self._type2_seq.to_bytes(1, byteorder='big') + bytes('{}\n@@@@@@@@@@@@@'.format(msg), 'utf-8')
        self._type2_seq = (self._type2_seq + 1) if (self._type2_seq < 0xFF) else 0x67
        ret = self._dev.write(0x02, out_data[:16])
        in_data = bytes()
        while True:
            ret = bytes(self._dev.read(0x81, 0x10))
            if len(ret) == 0:
                break
            in_data += ret
        return in_data

    def __init__(self):
        self._dev = usb.core.find(idVendor=0x03eb, idProduct=0x2013)
        if self._dev is None:
            logger.error('iaqstick: iAQ Stick not found')
            return
        self._intf = 0
        #self._type1_seq = 0x0001
        self._type2_seq = 0x67

        try:
            if self._dev.is_kernel_driver_active(self._intf):
                self._dev.detach_kernel_driver(self._intf)

            self._dev.set_configuration(0x01)
            usb.util.claim_interface(self._dev, self._intf)
            self._dev.set_interface_altsetting(self._intf, 0x00)

        except Exception as e:
            logger.error("iaqstick: init interface failed - {}".format(e))
        self.initialized = True
        self.update();

    def stop(self):
        if not self.initialized:
            return
        self.alive = False
        try:
            usb.util.release_interface(self._dev, self._intf)
        except Exception as e:
            logger.error("iaqstick: releasing interface failed - {}".format(e))

    def update(self):
        if not self.initialized:
            return
        try:
            meas = self.xfer_type2('*TR')
            ppm = int.from_bytes(meas[2:4], byteorder='little')
            logger.debug('iaqstick: ppm: {}'.format(ppm))
            self.ppm = ppm
        except Exception as e:
            logger.error("iaqstick: update failed - {}".format(e))


    def state_string(self):
        if self.ppm > 1500:
            return "Bad"
        elif self.ppm > 1000:
            return "Mediocre"
        elif self.ppm > 500:
            return "Decent"
        elif self.ppm > 400:
            return "Good"
        else:
            return "Invalid Measurement"



DOMAIN = 'rehau_state'

airsensor = AirSensor()

def setup(hass, config):
    hass.states.set('rehau.state', "Air Quality is " + airsensor.state_string())

    return True

async def cmd(cmd):
    print("Start: %s" % cmd)
    proc = await asyncio.create_subprocess_shell(
            cmd, stdin=None, stderr=None, stdout=PIPE)
    print("Proc created: %s" % cmd)
    out = await proc.stdout.read()
    print("Finish: %s" % cmd)
    return out

async def run_all():
    s1 = cmd("dmesg")
    s2 = cmd("cat /etc/issue")
#    air_cmd = cmd(airsensor_binary)
    results = await asyncio.wait_for(air_cmd, 30)

    for res in results:
        print(res)



if __name__ == "__main__":
    # execute only if run as a script

    loop = asyncio.get_event_loop()
    i = 5000
    while i > 0:
        airsensor.update()
        print(airsensor.ppm)
        #air_cmd = cmd(airsensor_binary)
        #loop.run_until_complete(air_cmd)
        i = i - 1
    loop.close()
