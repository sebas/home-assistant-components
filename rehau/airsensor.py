#!/usr/bin/env python3
# vim: set encoding=utf-8 tabstop=4 softtabstop=4 shiftwidth=4 expandtab
#########################################################################
#  Copyright 2013 Robert Budde                       robert@projekt131.de
#  Copyright 2017 Sebastian Kügler                          sebas@kde.org
#########################################################################
#  based on iAQ-Stick plugin for SmartHome.py.
#                                        http://mknx.github.io/smarthome/
#
#  This plugin is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This plugin is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this plugin. If not, see <http://www.gnu.org/licenses/>.
#########################################################################

#/etc/udev/rules.d/99-iaqstick.rules
#SUBSYSTEM=="usb", ATTR{idVendor}=="03eb", ATTR{idProduct}=="2013", MODE="666"
#udevadm trigger

import logging
import usb.core
import usb.util

logger = logging.getLogger('iAQ_Stick')

class AirSensor():

    initialized = False

    ppm = 0

    def xfer_type2(self, msg):
        out_data = bytes('@', 'utf-8') + self._type2_seq.to_bytes(1, byteorder='big') + bytes('{}\n@@@@@@@@@@@@@'.format(msg), 'utf-8')
        self._type2_seq = (self._type2_seq + 1) if (self._type2_seq < 0xFF) else 0x67
        ret = self._dev.write(0x02, out_data[:16])
        in_data = bytes()
        while True:
            ret = bytes(self._dev.read(0x81, 0x10))
            if len(ret) == 0:
                break
            in_data += ret
        return in_data

    def __init__(self):
        self._dev = usb.core.find(idVendor=0x03eb, idProduct=0x2013)
        if self._dev is None:
            logger.error('iaqstick: iAQ Stick not found')
            return
        self._intf = 0
        #self._type1_seq = 0x0001
        self._type2_seq = 0x67

        try:
            if self._dev.is_kernel_driver_active(self._intf):
                self._dev.detach_kernel_driver(self._intf)

            self._dev.set_configuration(0x01)
            usb.util.claim_interface(self._dev, self._intf)
            self._dev.set_interface_altsetting(self._intf, 0x00)

        except Exception as e:
            logger.error("iaqstick: init interface failed - {}".format(e))
        self.initialized = True
        self.update();

    def stop(self):
        if not self.initialized:
            return
        self.alive = False
        try:
            usb.util.release_interface(self._dev, self._intf)
        except Exception as e:
            logger.error("iaqstick: releasing interface failed - {}".format(e))

    def update(self):
        if not self.initialized:
            return
        try:
            meas = self.xfer_type2('*TR')
            ppm = int.from_bytes(meas[2:4], byteorder='little')
            logger.debug('iaqstick: ppm: {}'.format(ppm))
            self.ppm = ppm
        except Exception as e:
            logger.error("iaqstick: update failed - {}".format(e))


    def state_string(self):
        if self.ppm > 1500:
            return "Bad"
        elif self.ppm > 1000:
            return "Mediocre"
        elif self.ppm > 500:
            return "Decent"
        elif self.ppm > 400:
            return "Good"
        else:
            return "Invalid Measurement"

if __name__ == '__main__':
    #logging.basicConfig(level=logging.DEBUG)
    airsensor = AirSensor()
    airsensor.update()
    airsensor.stop()
    print("PPM: " + airsensor.state_string() + " (" + str(airsensor.ppm) + ")")
    print("PPM: " + str(airsensor.ppm) + ")")
