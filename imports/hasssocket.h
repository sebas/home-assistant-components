/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HASS_WSOCKET_H
#define HASS_WSOCKET_H

#include <QObject>
#include <QSslError>
#include <QWebSocket>

class QNetworkReply;

namespace HomeAssistant
{

class HassSocket: public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool loading READ loading NOTIFY loadingChanged)
    Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(QString states READ states NOTIFY statesUpdated)
    Q_PROPERTY(QString error READ error NOTIFY errorChanged)

public:
    explicit HassSocket(QObject *parent = nullptr);
    ~HassSocket();

    QString url() const;
    void setUrl(const QString &url);

    QString password() const;
    void setPassword(const QString &pass);

    QString states() const;
    bool loading() const;

    void setError(const QString &err);
    QString error() const;

    Q_INVOKABLE void init();
    Q_INVOKABLE void cancel();
    Q_INVOKABLE void saveCredentials();

Q_SIGNALS:
    void urlChanged() const;
    void pathChanged() const;
    void passwordChanged() const;
    void countChanged() const;
    void loadingChanged() const;
    void statesUpdated() const;
    void errorChanged() const;


public:
    int count() const;

private Q_SLOTS:
    void onConnected();
    void onTextMessageReceived(QString message);
    void onSslErrors(const QList<QSslError> &errors);

    void closed();


private:
    void connectSocket();
    void requestStates();
    void subscribeStateChanges();

    QWebSocket m_webSocket;
    QNetworkReply *m_reply = nullptr;

    QString m_url;
    QString m_password;
    QString m_states;
    bool m_loading;
    QString m_error;

    int id = 0;

};

} // namespace Solid

#endif

