import QtQuick 2.0
import QtQuick.Controls 1.0
import org.kde.homeassistant 1.0 as HomeAssistant


/**
 *
 * $ curl -X GET -H "x-ha-access: YOUR_PASSWORD" \
       -H "Content-Type: application/json" \
 *
 */

Rectangle {

    width: 800
    height: 600
    color: "lightgrey"

    HomeAssistant.RestHelper {
        id: rest_states
        //url: 'https://192.168.1.64:8123/api/states?api_password=XXXXXXXXXXXXX'
        url: 'https://192.168.1.64:8123/api/states'
        password: passwordtextfield.text

        onJsonUpdated: {
            var d = eval('new Object(' + rest_states.json + ')');
            print("Response:" + rest_states.json );
            print("Object: " + d.ip);
        }
    }

    MouseArea {

        anchors.fill: parent

        onClicked: {
            print("Click!")
            rest_states.fetch();
        }

    }

    Item {
        id: loginPage

        anchors.centerIn: parent

        TextField {
            id: baseUrltxt
            anchors {
                top: parent.top
                left: parent.left
            }
            text: rest_states.password
        }

        TextField {
            id: passwordtextfield
            anchors {
                top: baseUrltxt.bottom
                left: parent.left
            }
            text: rest_states.password
        }

        Label {
            anchors {
                top: passwordtextfield.bottom
                left: passwordtextfield.left
            }
            text: rest_states.error
        }

        CheckBox {
            anchors {
                top: passwordtextfield.top
                left: passwordtextfield.right
            }
            id: savecreds
            text: "Save credentials"

        }

        Button {
            anchors {
                top: passwordtextfield.bottom
                left: passwordtextfield.right
            }
            text: "Log in"
            onClicked: {
                if (savecreds.checked) {
                    rest_states.saveCredentials();
                }
                rest_states.fetch();
            }
        }
    }

    Component.onCompleted: {
    }
}
