import QtQuick 2.0
import QtQuick.Controls 2.0

import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.core 2.0

import org.kde.homeassistant 1.0 as HomeAssistant

Item {

    width: childrenRect.width
    height: childrenRect.height
    anchors.margins: units.gridUnit

    property alias password: passtext.text
    property alias baseurl: hostfield.text

    PlasmaExtras.Heading {
        text: "Login"
        anchors {
            left: hostfield.left
            bottom: hostfield.top
        }
    }

    Label {
        id: hostlabel
        anchors {
            verticalCenter: hostfield.verticalCenter
            right: hostfield.left
        }
        text: "Host:"
    }

    Label {
        id: passlabel
        anchors {
            verticalCenter: passtext.verticalCenter
            right: passtext.left
        }
        text: "Password:"
    }

    TextField {
        id: hostfield
        width: units.gridUnit * 12
        text: rest_states.url
    }

    TextField {
        id: passtext
        anchors {
            top: hostfield.bottom
            left: hostfield.left
            right: hostfield.right
        }
        text: rest_states.password
    }

    Label {
        id: errorlabel
        anchors {
            top: passtext.bottom
            left: passtext.left
        }
        text: rest_states.error
    }

    CheckBox {
        id: savecheck
        anchors {
            top: passtext.bottom
            left: hostfield.left
        }
        text: "Remember"
    }

    Button {
        id: loginbutton
        anchors {
            top: passtext.bottom
            left: savecheck.right
            right: hostfield.right
        }
        text: "Log in"
        onClicked: {
            rest_states.url = login.baseurl
            rest_states.password = login.password
            if (savecheck.checked) {
                rest_states.saveCredentials();
            }
            rest_states.fetch();
        }
    }


    Component.onCompleted: {
        if (rest_states.password != "" && rest_states.baseurl != "" && rest_states.path != "") {
            //rest_states.fetch();
        }
    }
}
