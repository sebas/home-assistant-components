/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HASS_RESTHELPER_H
#define HASS_RESTHELPER_H

#include <QObject>


class QNetworkReply;

namespace HomeAssistant
{

class RestHelperPrivate;

class RestHelper: public QObject
{
    Q_OBJECT

    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(bool loading READ loading NOTIFY loadingChanged)
    Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(QString json READ json NOTIFY jsonUpdated)
    Q_PROPERTY(QString error READ error NOTIFY errorChanged)

public:
    explicit RestHelper(QObject *parent = nullptr);
    ~RestHelper();

    QString path() const;
    void setPath(const QString &path);

    QString url() const;
    void setUrl(const QString &url);

    QString password() const;
    void setPassword(const QString &pass);

    QString json() const;
    bool loading() const;

    void setError(const QString &err);
    QString error() const;

    Q_INVOKABLE void fetch();
    Q_INVOKABLE void cancel();
    Q_INVOKABLE void saveCredentials();

Q_SIGNALS:
    void urlChanged() const;
    void pathChanged() const;
    void passwordChanged() const;
    void countChanged() const;
    void loadingChanged() const;
    void jsonUpdated() const;
    void errorChanged() const;

public:
    int count() const;

private:
    void requestJson();

    QNetworkReply *m_reply = nullptr;

    QString m_url;
    QString m_path;
    QString m_password;
    QString m_json;
    bool m_loading;
    QString m_error;

};

} // namespace Solid

#endif

