/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STATECHANGEEVENT_H
#define STATECHANGEEVENT_H

#include <QObject>
#include <QDateTime>
#include <QVariant>

class QNetworkReply;

namespace HomeAssistant
{

class EntityState: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString entityId READ entityId CONSTANT)
    Q_PROPERTY(QDateTime lastUpdated READ lastUpdated NOTIFY stateChanged)

public:
    explicit EntityState(QObject *parent = nullptr);
    ~EntityState();

    QString entityId() const;
    void setEntityId(const QString &entityId);

    QDateTime lastUpdated();
    void setLastUpdated(const QDateTime &dt);

    void addEvent(const QString &json);

Q_SIGNALS:
    void stateChanged();

private:
    QString m_entityId;
    QVariant m_state;
    QDateTime m_lastUpdated;
};

} // namespace HomeAssistant

#endif

