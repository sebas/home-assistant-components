/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hasssocket.h"

#include <KConfigGroup>
#include <KSharedConfig>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <QDebug>

namespace HomeAssistant
{



HassSocket::HassSocket(QObject *parent)
    : QObject(parent)
{
    KSharedConfigPtr config = KSharedConfig::openConfig(QStringLiteral("homeassistantrc"));
    auto cfg = config->group(QStringLiteral("Host"));
    m_password = cfg.readEntry(QStringLiteral("password"));
    m_url = cfg.readEntry(QStringLiteral("baseurl"));
}

HassSocket::~HassSocket()
{
}

QString HassSocket::url() const
{
    return m_url;
}

void HassSocket::setUrl(const QString &url)
{
    if (m_url != url) {
        qDebug() << "Set url" << url;
        m_url = url;
        emit urlChanged();
    }
}

QString HassSocket::password() const
{
    return m_password;
}

void HassSocket::setPassword(const QString &pass)
{
    if (m_password != pass) {
        m_password = pass;
        emit passwordChanged();
    }
}

void HassSocket::init()
{
    connectSocket();
}

void HassSocket::cancel()
{
    if (m_reply) {
        m_reply->abort();
        m_reply = nullptr;
    }
}

bool HassSocket::loading() const
{
    return m_loading;
}

QString HassSocket::states() const
{
    return m_states;
}

QString HassSocket::error() const
{
    return m_error;
}

void HassSocket::setError(const QString& err)
{
    if (m_error != err) {
        m_error = err;
        emit errorChanged();
    }
}

void HassSocket::saveCredentials()
{
    KSharedConfigPtr config = KSharedConfig::openConfig(QStringLiteral("homeassistantrc"));
    auto cfg = config->group(QStringLiteral("Host"));
    cfg.writeEntry(QStringLiteral("password"), m_password);
    cfg.writeEntry(QStringLiteral("baseurl"), m_url);
    config->sync();
}

void HassSocket::connectSocket()
{
    QString url = m_url + QStringLiteral("?api_password=") + m_password;
//     QString url = m_url;
    qDebug() << "WebSocket server:" << m_url;
    connect(&m_webSocket, &QWebSocket::connected, this, &HassSocket::onConnected);
    connect(&m_webSocket, &QWebSocket::sslErrors, this, &HassSocket::onSslErrors);
    connect(&m_webSocket, &QWebSocket::textMessageReceived, this, &HassSocket::onTextMessageReceived);
    connect(&m_webSocket, &QWebSocket::disconnected, this, &HassSocket::closed);
    m_webSocket.open(QUrl(url));
} // namespace HomeAssistant

void HassSocket::onConnected()
{
    qDebug() << "Connected!";
}

void HassSocket::onSslErrors(const QList<QSslError> &errors)
{
    Q_UNUSED(errors);
    qWarning() << "FIXME: Ignoring SSL errors, replace with loading private certificate.";
    /* // Something like this...
    QList<QSslCertificate> cert = QSslCertificate::fromPath(QLatin1String("server-certificate.pem"));
    QSslError error(QSslError::SelfSignedCertificate, cert.at(0));
    QList<QSslError> expectedSslErrors;
    expectedSslErrors.append(error);

    QWebSocket socket;
    socket.ignoreSslErrors(expectedSslErrors);
    socket.open(QUrl(QStringLiteral("wss://myserver.at.home")));
    */
    // WARNING: Never ignore SSL errors in production code.
    // The proper way to handle self-signed certificates is to add a custom root
    // to the CA store.
    qDebug() << "Ignoring ssl erorrs.";
    m_webSocket.ignoreSslErrors();
}

void HassSocket::closed()
{
    qDebug() << "Socket connection closed. Reason:" << m_webSocket.closeReason() << m_webSocket.errorString();
}


void HassSocket::onTextMessageReceived(QString message)
{
    qDebug() << "==============> OK Message received from server:" << message;
    QJsonDocument jdoc = QJsonDocument::fromJson(message.toUtf8());
    QString _type = jdoc.object().value(QStringLiteral("type")).toString();
    if (_type == QStringLiteral("auth_ok")) {
        qDebug() << "Authentication OK!";
        requestStates();
        subscribeStateChanges();
        return;

    } else if (_type == QStringLiteral("result")) {
        qDebug() << "===> RESULT" << jdoc.object().value(QStringLiteral("type"));
        QJsonArray res = jdoc.object().value(QStringLiteral("result")).toArray();
        QJsonDocument rdoc;
        rdoc.setArray(res);
        m_states = QString::fromUtf8(rdoc.toJson());
        emit statesUpdated();
        return;

    } else if (_type == QStringLiteral("event")) {
        qDebug() << "===> EVENT" << jdoc.object().value(QStringLiteral("type"));

        //m_json = QString::fromUtf8(jdoc.toJson());
        QJsonArray res = jdoc.object().value(QStringLiteral("result")).toArray();
    //     res.setObject(jdoc.object().value(QStringLiteral("result")).toObject());
        QJsonDocument rdoc;
        rdoc.setArray(res);
        m_states = QString::fromUtf8(rdoc.toJson());
        m_states = m_states.replace(QStringLiteral("\\n"), QString());
        emit statesUpdated();
        qDebug() << "Full JSON:" << m_states;
        return;

    }
    qDebug() << "Something unhandled!";
}

void HassSocket::requestStates()
{
        // Request states ...
        /*
        {
            "id": 19,
            "type": "get_states"
        }
        */

        QJsonObject state_request;
        id++;
        state_request.insert(QStringLiteral("id"), QJsonValue(id));
        state_request.insert(QStringLiteral("type"), QJsonValue(QStringLiteral("get_states")));

        QJsonDocument req_doc;
        req_doc.setObject(state_request);
        m_webSocket.sendTextMessage(QString::fromUtf8(req_doc.toJson()));
}

void HassSocket::subscribeStateChanges()
{
        // Subscribe to state change events
        /*
        {
            "id": 18,
            "type": "subscribe_events",
            // Optional
            "event_type": "state_changed"
        }
        */

        QJsonObject state_change;
        id++;
        state_change.insert(QStringLiteral("id"), QJsonValue(id));
        state_change.insert(QStringLiteral("type"), QJsonValue(QStringLiteral("subscribe_events")));
        state_change.insert(QStringLiteral("event_type"), QJsonValue(QStringLiteral("state_changed")));

        QJsonDocument sc_doc;
        sc_doc.setObject(state_change);
        m_webSocket.sendTextMessage(QString::fromUtf8(sc_doc.toJson()));
        //qDebug() << "OK Request sent:" << sc_doc.toJson();
}



} // ns
