/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "resthelper.h"
//#include "resthelper_p.h"

#include <KConfigGroup>
#include <KSharedConfig>

#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <QDebug>

namespace HomeAssistant
{



RestHelper::RestHelper(QObject *parent)
    : QObject(parent)
{
    KSharedConfigPtr config = KSharedConfig::openConfig(QStringLiteral("homeassistantrc"));
    auto cfg = config->group(QStringLiteral("Host"));
    m_password = cfg.readEntry(QStringLiteral("password"));
    m_url = cfg.readEntry(QStringLiteral("baseurl"));
}

RestHelper::~RestHelper()
{
}

int RestHelper::count() const
{
    return 2;
}

QString RestHelper::url() const
{
    return m_url;
}

void RestHelper::setUrl(const QString &url)
{
    if (m_url != url) {
        qDebug() << "Set url" << url;
        m_url = url;
        emit urlChanged();
    }
}

QString RestHelper::path() const
{
    return m_path;
}

void RestHelper::setPath(const QString &path)
{
    if (m_path != path) {
        qDebug() << "Set path" << path;
        m_path = path;
        emit pathChanged();
    }
}

QString RestHelper::password() const
{
    return m_password;
}

void RestHelper::setPassword(const QString &pass)
{
    if (m_password != pass) {
        m_password = pass;
        emit passwordChanged();
    }
}

void RestHelper::fetch()
{
    requestJson();
}

void RestHelper::cancel()
{
    if (m_reply) {
        m_reply->abort();
        m_reply = nullptr;
    }
}

bool RestHelper::loading() const
{
    return m_loading;
}

QString RestHelper::json() const
{
    return m_json;
}

QString RestHelper::error() const
{
    return m_error;
}

void RestHelper::setError(const QString& err)
{
    if (m_error != err) {
        m_error = err;
        emit errorChanged();
    }
}

void RestHelper::saveCredentials()
{
    KSharedConfigPtr config = KSharedConfig::openConfig(QStringLiteral("homeassistantrc"));
    auto cfg = config->group(QStringLiteral("Host"));
    cfg.writeEntry(QStringLiteral("password"), m_password);
    cfg.writeEntry(QStringLiteral("baseurl"), m_url);
    config->sync();
}

void RestHelper::requestJson()
{
    // TODO: This should be a lot more advanced...
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager, &QNetworkAccessManager::sslErrors, this, [this](QNetworkReply *reply, const QList<QSslError> &errors) {
            reply->ignoreSslErrors(errors);
        }
    );

    connect(manager, &QNetworkAccessManager::finished, this, [this](QNetworkReply* reply) {
            qDebug() << "Data is in";
            Q_ASSERT(m_reply == reply);
            if (reply->error() == QNetworkReply::NoError) {
                //success
                QString json = QString::fromUtf8(reply->readAll());
                qDebug() << "Success" << (json != m_json);
                if (json != m_json) {
                    m_json = json;
                    qDebug() << "jsonupdated" << m_json;
                    emit this->jsonUpdated();
                }
            } else {
                //failure
                setError(reply->errorString());
                qDebug() << "Failure" << m_error;

            }
            delete reply;
            m_reply = nullptr;
        }
    );
    // the HTTP request
    //QNetworkRequest req(QUrl(QStringLiteral("http://ip.jsontest.com/")));
    const QString fullUrl = QString(QStringLiteral("%1/api/%2")).arg(m_url, m_path);
    qDebug() << "Requesting..." << fullUrl << m_password << m_path;

    QNetworkRequest req(QUrl::fromUserInput(fullUrl));
    req.setHeader(QNetworkRequest::ContentTypeHeader, QByteArray("application/json"));
    req.setRawHeader(QByteArray("x-ha-access"), m_password.toUtf8());
    if (m_reply != nullptr) {
        delete m_reply;
        m_reply = nullptr;
    }
    m_reply = manager->get(req);

} // namespace HomeAssistant


} // ns
#include "resthelper.moc"
